const message = 'tjue hur mår du?'

const render = (term, timer = 100) => {
  const arr = term.split('')
  let index = 0
  let letter = ''

  setInterval(() => {
    if (index < arr.length) {
      index++
      letter = arr[index - 1]
      console.log(letter)
    }
  }, timer)

  return letter
}

console.log(render(message, 300))
