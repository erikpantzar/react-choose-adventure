import React, { Component } from 'react'
import Text from '../Components/Text'
import Button from '../Components/Button'
import { LETTER_TIMING } from '../Helpers/constants'
import './Chapter.css'

class Chapter extends Component {
  state = {
    isOptionsVisible: false,
  }

  toggleOptions = () => {
    this.setState({
      isOptionsVisible: true,
    })
  }

  render() {
    const { content, options } = this.props
    const times = content.map(c => c.split('').length * LETTER_TIMING)

    const totalTimes = times.reduce((a, b) => a + b)

    setTimeout(() => {
      this.toggleOptions()
    }, totalTimes)

    const getDelay = index => {
      if (!index) {
        return 0
      }
      return times.slice(0, index).reduce((a, b) => a + b)
    }

    // Skip render until finished.. oor?
    if (!times.length) {
      return false
    }

    return (
      <div className="Chapter">
        <div>
          {[...content].map((p, i) => (
            <Text key={i} content={p} delay={getDelay(i)} />
          ))}
        </div>

        {this.state.isOptionsVisible && (
          <footer className="Chapter-footer">
            {options ? (
              options.map((option, index) => (
                <Button key={index} type={option.type} to={option.to}>
                  {option.label}
                </Button>
              ))
            ) : (
              <Button type="primary" to="/">
                Enough..
              </Button>
            )}
          </footer>
        )}
      </div>
    )
  }
}

export default Chapter
