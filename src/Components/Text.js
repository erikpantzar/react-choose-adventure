import React, { Component } from 'react'
import { LETTER_INTERVAL } from '../Helpers/constants'

class Text extends Component {
  state = {
    content: '',
  }

  componentDidMount() {
    const { content, delay } = this.props

    setTimeout(() => {
      this.makeLine(content)
    }, delay)
  }

  makeLine = (line = 'String') => {
    const arr = ['<p>', ...line.split(''), '</p>']
    let index = 0
    let letter = ''

    setInterval(() => {
      if (index < arr.length) {
        letter = arr[index]
        index++
        const next = this.state.content + letter

        this.setState({
          content: next,
        })
      }
    }, LETTER_INTERVAL)
  }

  render() {
    const { content } = this.state

    const createMarkup = () => {
      return { __html: content }
    }

    return <div dangerouslySetInnerHTML={createMarkup()} />
  }
}

export default Text
