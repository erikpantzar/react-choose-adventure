import React from 'react'
import { Link } from 'react-router-dom'
import './Button.css'

const Button = ({ type = 'primary', to = '', children, ...attributes }) => {
  if (type === 'primary')
    return (
      <Link to={to} className="Button Button--primary" {...attributes}>
        {children}
      </Link>
    )
  if (type !== 'primary')
    return (
      <Link to={to} className="Button Button--ghost" {...attributes}>
        {children}
      </Link>
    )
}

export default Button
