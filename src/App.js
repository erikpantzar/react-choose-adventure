import React, { Fragment } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import TinderStory from './Stories/Tinder'

const App = () => (
  <Router>
    <Fragment>
      <Route
        exact
        path="/"
        render={() => {
          return (
            <div>
              <Link to="tinder">Tinder story</Link>
            </div>
          )
        }}
      />
      <Route path="/tinder" component={TinderStory} />
    </Fragment>
  </Router>
)

export default App
