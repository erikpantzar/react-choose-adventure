import React, { Component, Fragment } from 'react'
import { Route } from 'react-router-dom'
import Chapter from '../Components/Chapter'

const chapterOne = [
  'Det var en gång en tjej som hade ett namn. Hon var självständig, stark personlighet, mörk humor och god självkänsla. Väluppfostrad trevlig och nyfiken på livet.',
]

const chapterTwo = ['Åren gick o gick.', 'Ett två treeee']

class Tinder extends Component {
  render() {
    const { match } = this.props
    return (
      <Fragment>
        <Route
          exact
          path="/tinder"
          render={() => (
            <Chapter
              content={chapterOne}
              options={[
                { type: 'ghost', to: '/', label: `Enough.` },
                { type: 'primary', to: `${match.url}/1`, label: 'Go on..' },
              ]}
            />
          )}
        />
        <Route
          exact
          path={`${match.url}/1`}
          render={() => (
            <Chapter
              content={chapterTwo}
              options={[
                // { type: 'primary', to: `${match.url}/1`, label: 'Go on..' },
                { type: 'ghost', to: '/', label: `Enough.` },
              ]}
            />
          )}
        />
      </Fragment>
    )
  }
}

export default Tinder
